<?php
    use app\models\Emple;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\ActionColumn;
    use yii\grid\GridView;

    /** @var yii\web\View $this */
    /** @var yii\data\ActiveDataProvider $dataProvider */

    $this->title = 'Administrar Empleados';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="emple-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'emp_no',
            'apellido',
            'oficio',
            [
                'label' => 'Nombre del Departamento',
                'value' => 'deptNo.dnombre'
            ],
            //'dir',
            'fecha_alt',
            'deptNo.loc',
            
            //'salario',
            // 'comision',
            //'dept_no',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Emple $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_no' => $model->emp_no]);
                }
            ],
        ],
    ]); ?>
</div>
