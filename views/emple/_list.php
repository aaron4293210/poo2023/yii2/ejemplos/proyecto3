<?php
    use yii\helpers\Html;
?>

<div class="card text-center" style='min-height: 365px'>
    <div class="card-header bg-dark text-white">
        <div class="card-title">
            <h5 class="card-title">
                <?= 
                    Html::a("Empleado {$model->emp_no}", 
                        ['view', 'emp_no' => $model->emp_no], 
                        ['class' => 'link-info link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover text-decoration-none text-white'], 
                    )
                ?>
            </h5>
        </div>
    </div>

    <div class="card-body">
        <h5 class="card-title font-italic text-primary"><?= $model->oficio ?></h5>
        <p class="card-text lead text-muted"><?= $model->getAttributeLabel('apellido') ?>: <?= $model->apellido ?></p>

        <ul>
            <li class="list-group-item list-group-item-action" data-bs-toggle="tooltip" title="<?= $model->getAttributeLabel('deptNo.loc') ?>"><?= $model->deptNo->loc ?></li>
        </ul>

        <?= 
            Html::ul($model, [
                'class' => 'list-group list-group-flush', 
                "item" => function ($valor, $indice) {
                    if (!in_array($indice, ['emp_no', 'apellido', 'oficio']) && $valor) {

                        return "<li class='list-group-item list-group-item-action' data-bs-toggle='tooltip' title='{$indice}'>{$valor}</li>";
                    }
                }
            ]) 
        ?>
    </div>
</div>