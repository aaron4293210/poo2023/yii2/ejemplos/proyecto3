<?php
    use yii\helpers\Html;
?>

<div class="card text-center">
    <div class="card-header text-bg-dark">
        <h5 class="card-title">
            <?= 
                Html::a("Departamento {$model->dept_no}", 
                ['view', 'dept_no' => $model->dept_no], 
                ['class' => 'link-info link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover text-decoration-none text-white'], 
            )
            ?>
        </h5>
    </div>

    <div class="card-body">
        <h5 class="card-title font-italic text-primary"><?= $model->dnombre ?></h5>

        <ul class="list-group list-group-flush">
            <li class="list-group-item list-group-item-action" data-bs-toggle="tooltip" title="localidad"><?= $model->loc ?></li>
        </ul>
    </div>
</div>