<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var yii\web\View $this */
    /** @var app\models\Depart $model */
    /** @var yii\widgets\ActiveForm $form */

    $this->title = 'Crear Departamentos';
?>

<div class="depart-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dept_no')->textInput() ?>

    <?= $form->field($model, 'dnombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
