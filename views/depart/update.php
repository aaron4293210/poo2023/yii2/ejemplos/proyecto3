<?php
    use yii\helpers\Html;

    /** @var yii\web\View $this */
    /** @var app\models\Depart $model */

    $this->title = 'Actualizar Departamentos: ' . $model->dept_no;
    $this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['indexg']];
    $this->params['breadcrumbs'][] = ['label' => "Departamento {$model->dept_no}", 'url' => ['view', 'dept_no' => $model->dept_no]];
    $this->params['breadcrumbs'][] = 'Departamentos';
?>

<div class="depart-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
