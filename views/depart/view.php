<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    /** @var yii\web\View $this */
    /** @var app\models\Depart $model */

    $this->title = "Actualizar Departamento: {$model->dept_no}";
    $this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>

<div class="depart-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'dept_no' => $model->dept_no], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'dept_no' => $model->dept_no], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estas seguro que quieres borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dept_no',
            'dnombre',
            'loc',
        ],
    ]) ?>
</div>
