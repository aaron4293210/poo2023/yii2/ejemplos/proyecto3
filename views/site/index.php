<?php
    /** @var yii\web\View $this */

    $this->title = 'Departamentos';
?>
    <div class="site-index">
        <div class="jumbotron text-center bg-transparent mt-5 mb-5">
            <h1 class="display-4">Departamentos!</h1>
        </div>

    <div class="body-content text-center">
        <div class="row">
            <div class="col-lg-6 mb-3">
                <h2>Departamentos</h2>

                <p>
                    <a class="btn btn-outline-info" href="depart">ListView &raquo;</a>
                    <a class="btn btn-outline-info" href="/depart/indexg">GridView &raquo;</a>
                </p>
            </div>

            <div class="col-lg-6 mb-3">
                <h2>Empleados</h2>

                <p>
                    <a class="btn btn-outline-info" href="/emple">ListView &raquo;</a>
                    <a class="btn btn-outline-info" href="/emple/indexg">GridView &raquo;</a>
                </p>
            </div>
        </div>
    </div>
</div>
