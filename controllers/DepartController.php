<?php
    namespace app\controllers;

    use app\models\Depart;
    use yii\data\ActiveDataProvider;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    /**
     * DepartController implements the CRUD actions for Depart model.
     */
    class DepartController extends Controller {

        /**
         * @inheritDoc
        */
        public function behaviors() {
            return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
            );
        }

        /**
         * Lists all Depart models.
         *
         * @return string
        */
        public function actionIndex() {
            $dataProvider = new ActiveDataProvider([
                'query' => Depart::find(),
                'pagination' => [
                    'pageSize' => 4
                ],
                'sort' => [
                    'defaultOrder' => [
                        'dept_no' => SORT_DESC,
                    ]
                ],
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionIndexg() {
            $dataProvider = new ActiveDataProvider([
                'query' => Depart::find(),

                'pagination' => [
                    'pageSize' => 5
                ],
                'sort' => [
                    'defaultOrder' => [
                        'dept_no' => SORT_DESC,
                    ]
                ],
            ]);

            return $this->render('indexg', [
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single Depart model.
         * @param int $dept_no Dept No
         * @return string
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($dept_no)
        {
            return $this->render('view', [
                'model' => $this->findModel($dept_no),
            ]);
        }

        /**
         * Creates a new Depart model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return string|\yii\web\Response
         */
        public function actionCreate()
        {
            $model = new Depart();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'dept_no' => $model->dept_no]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }

        /**
         * Updates an existing Depart model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param int $dept_no Dept No
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($dept_no)
        {
            $model = $this->findModel($dept_no);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dept_no' => $model->dept_no]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing Depart model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param int $dept_no Dept No
         * @return \yii\web\Response
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($dept_no)
        {
            $this->findModel($dept_no)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the Depart model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param int $dept_no Dept No
         * @return Depart the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($dept_no)
        {
            if (($model = Depart::findOne(['dept_no' => $dept_no])) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
